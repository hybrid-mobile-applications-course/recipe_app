import Navigator from './navigation/RootStack';
import React from 'react';
import {YellowBox} from 'react-native';

export default function App() {
  YellowBox.ignoreWarnings([
    'Warning: Each child in a list should have a unique',
  ]);
  return <Navigator />;
}
