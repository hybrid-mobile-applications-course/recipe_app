import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import BreakfastScreen from '../screens/BreakfastScreen';
import LunchScreen from '../screens/LunchScreen';
import DinnerScreen from '../screens/DinnerScreen';
import DetailsScreen from '../screens/DetailsScreen';
import CommentsScreen from '../screens/CommentsScreen';
import {createAppContainer} from 'react-navigation';

const RootStack = createStackNavigator({
  Recipes: createBottomTabNavigator({
    Breakfast: {
      screen: BreakfastScreen,
    },
    Lunch: {
      screen: LunchScreen,
    },
    Dinner: {
      screen: DinnerScreen,
    },
  }),
  DetailsScreen: {
    screen: DetailsScreen,
  },
  CommentsScreen: {
    screen: CommentsScreen,
  },
});

export default createAppContainer(RootStack);
