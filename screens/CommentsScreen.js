import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Button,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

export default class CommentsScreen extends React.Component {
  _isMounted = false;

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  constructor() {
    super();
    this.state = {
      list: [],
      myComment: '',
      newComment: '',
    };
  }

  getComment = async () => {
    try {
      let savedComment = await AsyncStorage.getItem(
        this.props.navigation.state.params,
      );
      if (savedComment !== null) {
        if (this._isMounted) {
          this.setState({list: JSON.parse(savedComment)});
        }
      }
    } catch (e) {}
  };

  storeComment = async () => {
    console.log('Store Comment');
    let arrayOfComments = this.state.list;
    arrayOfComments.push(`${this.state.newComment}`);
    const stringedArray = JSON.stringify(arrayOfComments);
    try {
      await AsyncStorage.setItem(
        this.props.navigation.state.params,
        stringedArray,
      );
    } catch (e) {}
    await this.getComment();
  };

  deleteSingleComment(index) {
    this.state.list.splice(index, 1);
    try {
      AsyncStorage.setItem(
        this.props.navigation.state.params,
        JSON.stringify(this.state.list),
      );
    } catch (e) {}
  }

  deleteAllComments = () => {
    console.log('On Clear Array');
    this.setState({list: []}, () => {
      try {
        AsyncStorage.setItem(
          this.props.navigation.state.params,
          JSON.stringify(this.state.list),
        );
      } catch (e) {}
    });
  };

  showEntriesWithButtons() {
    return this.state.list.map((item, index) => (
      <>
        <View style={styles.buttonStyleContainer}>
          <Text style={styles.actualComments} key={index}>
            {item}
          </Text>
          <Button
            title={'X'}
            onPress={this.deleteSingleComment.bind(this, index)}
          />
        </View>
      </>
    ));
  }

  render() {
    this.getComment();
    return (
      <ScrollView style={{padding: 10}}>
        <TextInput
          multiline={true}
          style={{height: 40}}
          placeholder="Type your comment here"
          placeholderTextColor="#9a73ef"
          onChangeText={text => this.setState({newComment: text})}
        />
        <Button title={'Leave comment'} onPress={this.storeComment} />
        <Text style={styles.oneMoreStyle}>
          Our users say about {this.props.navigation.state.params}:
        </Text>
        {this.showEntriesWithButtons()}
        <Button title={'Delete all'} onPress={this.deleteAllComments} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    padding: 10,
  },
  commentTextInputStyle: {
    height: 40,
  },
  oneMoreStyle: {
    paddingTop: 30,
    justifyContent: 'center',
    marginLeft: '4%',
    fontWeight: 'bold',
  },
  actualComments: {
    paddingTop: 9,
    marginLeft: '2%',
    fontStyle: 'italic',
    lineHeight: 20,
    paddingBottom: 20,
    fontSize: 20,
  },
  buttonStyleContainer: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 5,
    marginTop: 5,
  },
});
