import React from 'react';
import {Button, Image, ScrollView, StyleSheet, Text} from 'react-native';

export default class DetailsScreen extends React.Component {
  item = this.props.navigation.state.params.item;
  title = this.props.navigation.state.params.item.title;
  preview = this.props.navigation.state.params.item.preview;
  ingredients = this.props.navigation.state.params.item.ingredients;
  guide = this.props.navigation.state.params.item.guide;

  render() {
    return (
      <ScrollView style={styles.containerForRecipe}>
        <Image style={styles.cardImage} source={{uri: this.preview}} />
        <Button
          title={'Leave comment'}
          onPress={() =>
            this.props.navigation.navigate('CommentsScreen', this.title)
          }
        />
        <Text style={styles.textInsideRecipeName}>{this.title}</Text>
        <Text style={styles.textInsideRecipeIngredientsName}>Ingredients:</Text>
        <Text style={styles.textInsideRecipeIngredients}>
          {this.ingredients}
        </Text>
        <Text style={styles.textInsideRecipeGuideName}>Preparation:</Text>
        <Text style={styles.textInsideRecipeGuide}>{this.guide}</Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerForRecipe: {
    marginHorizontal: 20,
  },
  cardImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  textInsideRecipeName: {
    padding: 10,
    fontSize: 20,
    fontWeight: 'bold',
  },
  textInsideRecipeIngredientsName: {
    padding: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
  textInsideRecipeIngredients: {
    padding: 10,
    fontSize: 16,
  },
  textInsideRecipeGuideName: {
    padding: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
  textInsideRecipeGuide: {
    padding: 10,
    fontSize: 16,
  },
});
